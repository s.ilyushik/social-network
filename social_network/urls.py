"""social_network URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

from django.conf import settings
from django.conf.urls.static import static
from .yasg import urlpatterns as doc_urls

from users.users_api.views import CustomTokenObtainPairView

urlpatterns = [
    path('', include('common.urls')),
    path('comments/', include('comments.urls')),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('auth/jwt/create/', CustomTokenObtainPairView.as_view()),
    path('auth/', include('djoser.urls.jwt')),
    path('users/', include('users.urls')),
    path('users/', include('django.contrib.auth.urls')),
    path('api/', include('common.common_api.urls', namespace='api')),
    path('api/', include('users.users_api.urls')),
    path('api/', include('comments.comments_api.urls')),
]

urlpatterns += doc_urls

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
