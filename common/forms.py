from django import forms
from django.core.exceptions import ValidationError

from .models import Post


class PostForm(forms.ModelForm):
    """
    Main form of user posts creating
    """
    class Meta:
        model = Post
        fields = ['body', 'image', 'interests']
        widgets = {
            'body': forms.Textarea(attrs={'class': 'form-control', 'rows': '3'}),
            'image': forms.ClearableFileInput(attrs={'class': 'form-control-file'}),
            'interests': forms.CheckboxSelectMultiple()
        }

    def clean(self):
        cleaned_data = super(PostForm, self).clean()
        body = self.cleaned_data.get('body')
        image = self.cleaned_data.get('image')

        if body == '' and not image:
            raise ValidationError('You cannot create an empty post (in which there is no image or text)!')
        return cleaned_data
