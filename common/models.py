import uuid
from django.db import models
from django.shortcuts import reverse
from django.utils.text import slugify

from settings.base import AUTH_USER_MODEL

from common.utils import slug_generator


class Post(models.Model):
    """Users Post Model"""
    slug = models.SlugField(max_length=150, blank=True, unique=True)
    body = models.TextField(blank=True, db_index=True, verbose_name="Write your post")
    date_time = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(
        upload_to='users_uploads/posts_images',
        blank=True,
        verbose_name="Upload a picture for your post"
    )
    interests = models.ManyToManyField(
        'Interest', blank=True,
        related_name='posts',
        verbose_name="Choose interests for your post"
    )
    username = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='author', related_name='post')
    likes = models.ManyToManyField(AUTH_USER_MODEL, blank=True, verbose_name='Liker', related_name='posts_was_liked')

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slug_generator(self.body[:20], self.username)
        super().save(*args, **kwargs)

    def get_update_url(self):
        return reverse('post_update_url', kwargs={'slug': self.slug})

    def __str__(self):
        return '{}'.format(self.slug)

    class Meta:
        ordering = ('-date_time',)


class Interest(models.Model):
    """Interest's Model"""
    slug = models.SlugField(max_length=50, blank=True, unique=True)
    title = models.CharField(max_length=50, db_index=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title[:20]) + '_' + str(uuid.uuid4())
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('interest_detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return '{}'.format(self.title)


class PrivacyPolicy(models.Model):
    """Privacy Policy of Project Model"""
    title = models.CharField(max_length=50)
    version = models.CharField(max_length=20)
    body = models.TextField(max_length=20000)
    current_version = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.title)
