from django.utils.text import slugify
from time import time


def slug_generator(raw_symbols, author_usermame):
    """
    Generator for Post's slug
    """
    main_slug_symbols = slugify(raw_symbols, allow_unicode=True)
    author_name = slugify(author_usermame, allow_unicode=True)
    return main_slug_symbols + '-' + author_name + '-' + str(int(time()))
