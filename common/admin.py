from django.contrib import admin
from common.models import Post, Interest, PrivacyPolicy
from comments.models import Comment


class CommentInline(admin.TabularInline):
    model = Comment


class PostAdmin(admin.ModelAdmin):
    inlines = [
        CommentInline,
    ]


class InterestAdmin(admin.ModelAdmin):
    exclude = ('slug',)


admin.site.register(Post, PostAdmin)
admin.site.register(Interest, InterestAdmin)
admin.site.register(PrivacyPolicy)