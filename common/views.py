from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.core.paginator import Paginator

from .models import Post, Interest
from django.contrib.auth import get_user_model
from .forms import PostForm
from comments.forms import CommentForm
from users.forms import InterestForm


class PostsList(ListView):
    """All posts list or posts list search result And list pagination"""
    model = Post
    template_name = 'common/index.html'

    def get_context_data(self, **kwargs):
        form = PostForm()
        form_of_comment = CommentForm(auto_id=False)
        form_of_interest = InterestForm()
        context = super().get_context_data(**kwargs)
        context['form'] = form
        context['form_of_comment'] = form_of_comment
        context['form_of_interest'] = form_of_interest
        # Search posts with pagination
        search_query = self.request.GET.get('search', '')
        if search_query:
            posts = Post.objects.filter(
                Q(body__icontains=search_query) |
                Q(username__username__icontains=search_query) |
                Q(username__last_name__icontains=search_query)
            ).select_related("username")
            # Objects per search page
            self.pagination(context, 3, posts)
            if context['object_list']:
                context['search_message_success'] = 'Posts containing ' + '"' + search_query + '":'
            else:
                context['search_message_no_matches'] = 'No posts containing' + ' "' + search_query + '" found.'
        # All posts with pagination
        else:
            posts = Post.objects.all()
            # Objects per posts page
            self.pagination(context, 3, posts)
        return context

    def pagination(self, context, number_obj_per_page, posts):
        """Main pagination method (for all posts objects or searched post objects)"""
        search_query = self.request.GET.get('search', '')
        paginator = Paginator(posts, number_obj_per_page)
        page_number = self.request.GET.get('page', 1)
        page = paginator.get_page(page_number)
        context['is_paginated'] = page.has_other_pages()
        if page.has_previous() and not search_query:
            context['prev_url'] = '?page={}'.format(page.previous_page_number())
        elif page.has_previous() and search_query:
            context['prev_url'] = '?search={}&page={}'.format(search_query, page.previous_page_number())
        else:
            context['prev_url'] = ''
        if page.has_next() and not search_query:
            context['next_url'] = '?page={}'.format(page.next_page_number())
        elif page.has_next() and search_query:
            context['next_url'] = '?search={}&page={}'.format(search_query, page.next_page_number())
        else:
            context['next_url'] = ''
        context['object_list'] = page


class PostDetail(DetailView):
    model = Post
    template = 'common/post_detail.html'

    def get_context_data(self, **kwargs):
        form_of_comment = CommentForm(auto_id=False)
        context = super().get_context_data(**kwargs)
        context['form_of_comment'] = form_of_comment
        return context


class PostCreate(LoginRequiredMixin, CreateView):
    model = Post
    form_class = PostForm
    template_name = 'common/index.html'
    success_url = reverse_lazy('posts_list_url')
    login_url = 'login'

    def form_valid(self, bound_form):
        bound_form.instance.username = self.request.user
        bound_form.save()
        return super().form_valid(bound_form)


class PostUpdate(LoginRequiredMixin, UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'common/post_update_form.html'
    login_url = 'login'

    def form_valid(self, bound_form):
        bound_form.username = self.request.user
        return super().form_valid(bound_form)


class PostDelete(LoginRequiredMixin, DeleteView):
    model = Post
    template_name = 'common/index.html'
    success_url = reverse_lazy('posts_list_url')
    login_url = 'login'


class LikesDislikes(LoginRequiredMixin, UpdateView):
    """
    Post's like button and dislike button.

    Inverting logic and appearance after clicking.
    """
    model = Post
    login_url = 'login'

    def _like(self, request):
        try:
            user_pk, post_object = self.get_objects_and_args(request)
            post_object.likes.add(get_user_model().objects.get(pk=user_pk))
            message = 'like_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _dislike(self, request):
        try:
            user_pk, post_object = self.get_objects_and_args(request)
            post_object.likes.remove(get_user_model().objects.get(pk=user_pk))
            message = 'dislike_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    # Retrieving values for use in the methods of this view
    def get_objects_and_args(self, request):
        user_pk = self.request.user.pk
        post_object = Post.objects.get(slug__iexact=request.POST.get('object_slug'))
        return user_pk, post_object

    @method_decorator(require_POST)
    def post(self, request, *args, **kwargs):
        user_pk, post_object = self.get_objects_and_args(request)

        if get_user_model().objects.get(pk=user_pk) not in post_object.likes.all():
            return self._like(request)

        if get_user_model().objects.get(pk=user_pk) in post_object.likes.all():
            return self._dislike(request)
        return JsonResponse({'status': 'error', 'reason': 'Bad request'}, status=400)


class InterestsList(ListView):
    model = Interest
    template_name = 'common/interests_list.html'


class InterestDetail(DetailView):
    model = Interest
    template = 'common/interest_detail.html'
