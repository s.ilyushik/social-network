from django import template
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from common.models import PrivacyPolicy

register = template.Library()


# Filters for friendship manage buttons displaying in posts
@register.filter
def filter_add_to_friends_from_post(user, post):
    return user.id != post.username.id and post.username not in user.friends.all() and user not in post.username.friendship_offers.all() and post.username not in user.friendship_offers.all()

@register.filter
def filter_cancel_friends_request_from_post(user, post):
    return user in post.username.friendship_offers.all() and post.username not in user.friendship_offers.all()

@register.filter
def accept_friendship_offer_from_post(user, post):
    return post.username in user.friendship_offers.all() and user not in post.username.friendship_offers.all()

@register.filter
def stop_friendship_from_post(user, post):
    return post.username in user.friends.all() and user in post.username.friends.all()


# Filters for friendship manage buttons displaying in comments
@register.filter
def filter_add_to_friends_from_comment(user, comment):
    return user.id != comment.author.id and comment.author not in user.friends.all() and user not in comment.author.friendship_offers.all() and comment.author not in user.friendship_offers.all()

@register.filter
def filter_cancel_friends_request_from_comment(user, comment):
    return user in comment.author.friendship_offers.all() and comment.author not in user.friendship_offers.all()

@register.filter
def accept_friendship_offer_from_comment(user, comment):
    return comment.author in user.friendship_offers.all() and user not in comment.author.friendship_offers.all()

@register.filter
def stop_friendship_from_comment(user, comment):
    return comment.author in user.friends.all() and user in comment.author.friends.all()


# Filters for friendship manage buttons displaying in user profile page
@register.filter
def filter_add_to_friends_from_user_profile(user, customuser):
    return user.id != customuser.id and customuser not in user.friends.all() and user not in customuser.friendship_offers.all() and customuser not in user.friendship_offers.all()

@register.filter
def filter_cancel_friends_request_from_user_profile(user, customuser):
    return user in customuser.friendship_offers.all() and customuser not in user.friendship_offers.all()

@register.filter
def accept_friendship_offer_from_user_profile(user, customuser):
    return customuser in user.friendship_offers.all() and user not in customuser.friendship_offers.all()

@register.filter
def stop_friendship_from_user_profile(user, customuser):
    return customuser in user.friends.all() and user in customuser.friends.all()


# Add the current Privacy Policy object in a template
@register.simple_tag()
def get_privacy_policy():
    try:
        return PrivacyPolicy.objects.get(current_version=True)
    except MultipleObjectsReturned:
        message = 'Site administrator marked more than one version as current or did not fill out body text'
        return message
    except ObjectDoesNotExist:
        message = 'Site administrator did not mark any version as current or did not fill out body text'
        return message