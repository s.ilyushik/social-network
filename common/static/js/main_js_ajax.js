$(document).ready(function () {


    // Getting a csrf token from a cookie
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');


    function csrfSafeMethod(method) {
        // These HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    // Call modal window for post delete confirmation
    $(".post_delete_btn").click(function() {
        $(".modal_post_delete_confirmation").modal('show');
    });


    // Ajax post delete
    $(".delete_post_confirmation_btn").on("click", function (e) {

        e.preventDefault();
        var slug = $(".delete_post_confirmation_btn").attr('data-slug');

        $.ajax({
            method: "POST",
            url: window.location.origin + '/post/'+slug+'/delete/',
            data: slug,
            success: function(){
               $(".post"+slug).remove();
            },
            error: function (data) {console.log(data);}
        });
    });


    // Ajax post detail
    $(".post_detail_btn").on("click", function (e) {

        e.preventDefault();
        var slug = $(".post_detail_btn").attr('data-slug');

        $.ajax({
            url: window.location.origin + '/post/'+slug+'/',
            data: slug,
            success: function (data) {
                $(location).attr('href', '/post/'+slug+'/');
            },
            error: function (data) {console.log(data);}
        });
    });


    //Bootstrap tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
});