$(document).ready(function () {


    // Like / Dislike functionality
    $('.like_btn').on("click", function (e) {
        e.preventDefault();
        var object_slug = $(this).attr('data-slug');
        // To use a fixed value in $.ajax
        var button = $(this);

        $.ajax({
            method: "POST",
            url: window.location.origin + '/likes/',
            data: {object_slug: object_slug},
            success: function(data){
                if (data.data === 'like_done') {
                    button.children().attr('src', '/static/images/ic_thumb_down_24px.png').text('Dislike');
                    // Increase the counter of likes after pressing the button
                    $('.likes_count_' + object_slug).html(Number($('.likes_count_' + object_slug).html()) + 1);
                }
                if (data.data === 'dislike_done') {
                    button.children().attr('src', '/static/images/ic_thumb_up_24px.png').text('Like');
                    // Decrease the counter of likes after pressing the button
                    $('.likes_count_' + object_slug).html(Number($('.likes_count_' + object_slug).html()) - 1);
                }
                },
            error: function (data) {console.log(data);}
        });
    })
});