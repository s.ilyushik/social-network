from django.test import TestCase

from common.forms import PostForm
from common.models import Post


class TestPostForm(TestCase):
    """Tests for main post creating form"""

    def test_post_form_field_labels(self):
        form = PostForm()
        models_field_verbose_name = Post._meta.get_field('body').verbose_name
        self.assertTrue(
            form.fields['body'].label == models_field_verbose_name)
        models_field_verbose_name = Post._meta.get_field('image').verbose_name
        self.assertTrue(
            form.fields['image'].label == models_field_verbose_name)
        models_field_verbose_name = Post._meta.get_field('interests').verbose_name
        self.assertTrue(
            form.fields['interests'].label == models_field_verbose_name)

    def test_post_form_attrs(self):
        form = PostForm()
        self.assertEqual(
            form.fields['body'].widget.attrs['class'],
            'form-control',
            'Widget attr class not form-control'
        )
        self.assertEqual(form.fields['body'].widget.attrs['rows'], '3', 'Widget attr rows not 3')
        self.assertEqual(
            form.fields['image'].widget.attrs['class'],
            'form-control-file',
            'Widget attr class not form-control-file'
        )

    def test_post_form_if_not_both_body_image(self):
        form_data = {'body': '', 'image': ''}
        form = PostForm(data=form_data)
        self.assertFalse(form.is_valid(), 'The validator missed an invalid value!')
        # Correct validator message
        self.assertEqual(
            form.errors['__all__'],
            ['You cannot create an empty post (in which there is no image or text)!'],
            'The validator message does not match the one set in the form!'
        )
