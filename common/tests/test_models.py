from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.test import TestCase
from django.urls import reverse

from common.models import Interest,  Post
from users.models import CustomUser


class TestInterestModel(TestCase):
    """Test Interest Model Methods"""
    @classmethod
    def setUpTestData(cls):
        Interest.objects.create(title='Title text test')

    def test_objects_field_not_none(self):
        interest = Interest.objects.get(id=1)
        self.assertIsNotNone(interest.slug, 'The slug was not automatically generated!')

    def test_unique_fields_val(self):
        with self.assertRaises(ValidationError):
            interest_slug = Interest.objects.get(id=1).slug
            Interest.objects.create(title='Title text test', slug=interest_slug).full_clean()

    def test_fields_max_length(self):
        with self.assertRaises(ValidationError):
            # Slug max_length 50 (this is 51)
            Interest.objects.create(slug='slug_test_unique1'*3).full_clean()
        with self.assertRaises(ValidationError):
            # Title max_length 50 (this is 52)
            Interest.objects.create(title='Title_text_test_unique_001'*3).full_clean()

    def test_get_absolute_url(self):
        interest = Interest.objects.get(id=1)
        self.assertEquals(
            interest.get_absolute_url(),
            reverse('interest_detail_url', args=[interest.slug]),
            'Incorrect value of absolute url'
        )


class TestPostModel(TestCase):
    """Test Post Model Methods"""
    @classmethod
    def setUpTestData(cls):
        username_obj = CustomUser.objects.create(username='Tester_name')
        Post.objects.create(slug='slug_test', username=username_obj)

    def test_get_absolute_url(self):
        post = Post.objects.get(id=1)
        # Unique generated slug
        slug = post.slug
        self.assertEquals(
            post.get_absolute_url(),
            reverse('post_detail_url', args=[slug]),
            'Incorrect value of absolute url'
        )
