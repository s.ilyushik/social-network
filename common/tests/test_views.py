from django.test import TestCase
from django.urls import reverse

from common.models import Post, Interest
from users.models import CustomUser


class TestPostListView(TestCase):
    """Test PostsList View Methods"""
    @classmethod
    def setUpTestData(cls):
        # Create 11 posts for tests
        number_of_posts = 11
        username_obj = CustomUser.objects.create_user(username='Tester_name', email='11@11.com', password=1234567)
        for post_num in range(number_of_posts):
            Post.objects.create(body='Body test text %s' % post_num, username=username_obj)

    def test_view_existence_of_forms(self):
        resp = self.client.get(reverse('posts_list_url'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['form'], 'Response has no form of post creating!')
        self.assertTrue(resp.context['form_of_comment'], 'Response has no form of comment creating!')
        self.assertTrue(resp.context['form_of_interest'], 'Response has no form of choosing interests!')

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('posts_list_url'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/index.html')

    def test_pagination_is_three(self):
        resp = self.client.get(reverse('posts_list_url'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] is True)
        self.assertTrue(len(resp.context['object_list']) == 3)

    def test_second_page_pagination_is_three(self):
        # Get second page and confirm it has (exactly) remaining 3 items
        resp = self.client.get(reverse('posts_list_url') + '?page=2')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('is_paginated' in resp.context)
        self.assertTrue(resp.context['is_paginated'] is True)
        self.assertTrue(len(resp.context['object_list']) == 3)

    def test_search_did_not_any_result(self):
        resp = self.client.get(reverse('posts_list_url') + '?search=Other_tester_name')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['search_message_no_matches'])

    def test_search_success_result(self):
        resp = self.client.get(reverse('posts_list_url') + '?search=Tester_name')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['search_message_success'])


class TestPostDetail(TestCase):
    """Test PostDetail View Methods"""
    @classmethod
    def setUpTestData(cls):
        # Create 2 posts for tests
        number_of_posts = 2
        username_obj = CustomUser.objects.create(username='Tester_name')
        for post_num in range(number_of_posts):
            Post.objects.create(body='Body test text %s' % post_num, username=username_obj)

    def test_view_uses_correct_template(self):
        post = Post.objects.get(pk=1)
        resp = self.client.get('/post/' + post.slug + '/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/post_detail.html')

    def test_view_existence_of_form(self):
        post = Post.objects.get(pk=1)
        resp = self.client.get('/post/' + post.slug + '/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['form_of_comment'], 'Response has no form of comment creating!')


class TestPostCreateView(TestCase):
    """Test PostCreate View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_user2 = CustomUser.objects.create_user(username='testuser2', email='222@22.com', password='1234567')
        test_user2.save()
        test_post1 = Post.objects.create(body='Body test', username=test_user1)
        test_post1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('post_create_url'))
        self.assertRedirects(resp, '/users/login/?next=/post/create/')

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='testuser1', password='1234567')
        resp = self.client.get(reverse('posts_list_url'))
        # Check the user is logged in
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/index.html')

    def test_username_in_post_is_specified(self):
        self.client.login(username='testuser1', password='1234567')
        user = CustomUser.objects.get(pk=1)
        # Create a post in a database with pk=2
        resp = self.client.post(reverse('post_create_url'), {'body': 'lorem text', 'username': user})
        post = Post.objects.get(pk=2)
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/')
        self.assertEqual(
            user,
            post.username,
            'Post username does not match the username when it was created or is missing!'
        )

    def test_view_existence_of_form(self):
        self.client.login(username='testuser1', password='1234567')
        resp = self.client.get('/post/create/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['form'], 'Response has no a post create form!')


class TestPostUpdateView(TestCase):
    """Test PostUpdate View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_post1 = Post.objects.create(body='Body test', username=test_user1)
        test_post1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        user = CustomUser.objects.get(pk=1)
        post = Post.objects.get(pk=1)
        resp = self.client.post('/post/' + post.slug + '/update/', {'body': 'lorem text', 'username': user})
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/post/' + post.slug + '/update/')

    def test_redirect_if_logged_in(self):
        self.client.login(username='testuser1', password='1234567')
        post = Post.objects.get(pk=1)
        # Editing the body text of the post
        resp = self.client.post('/post/' + post.slug + '/update/', {'body': 'lorem updated text'})
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/post/' + post.slug + '/')

    def test_view_existence_of_form(self):
        self.client.login(username='testuser1', password='1234567')
        post = Post.objects.get(pk=1)
        resp = self.client.get('/post/' + post.slug + '/update/')
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['form'], 'Response has no a post edit form!')

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='testuser1', password='1234567')
        post = Post.objects.get(pk=1)
        resp = self.client.get('/post/' + post.slug + '/update/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/post_update_form.html')


class TestPostDeleteView(TestCase):
    """Test PostDelete View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_post1 = Post.objects.create(body='Body test', username=test_user1)
        test_post1.save()
        self.client.logout()

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='testuser1', password='1234567')
        post = Post.objects.get(pk=1)
        resp = self.client.get(reverse('post_delete_url', args=[post.slug]))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/index.html')

    def test_redirect_if_not_logged_in(self):
        post = Post.objects.get(pk=1)
        resp = self.client.post(reverse('post_delete_url', args=[post.slug]))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/post/' + post.slug + '/delete/')

    def test_redirect_if_logged_in(self):
        self.client.login(username='testuser1', password='1234567')
        post = Post.objects.get(pk=1)
        resp = self.client.post(reverse('post_delete_url', args=[post.slug]))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/')


class TestLikesDislikes(TestCase):
    """Test LikesDislikes View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_user2 = CustomUser.objects.create_user(username='testuser2', email='222@22.com', password='1234567')
        test_user2.save()
        test_post1 = Post.objects.create(body='Body test', username=test_user1)
        test_post1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.post(reverse('post_likes'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/likes/')

    def test_incorrect_slug_to_like_view(self):
        self.client.login(username='testuser2', password='1234567')
        with self.assertRaises(Post.DoesNotExist):
            self.client.post('/likes/', data={'object_slug': 'my_does_not_exist_slug'})

    def test_like_success(self):
        self.client.login(username='testuser2', password='1234567')
        post_object = Post.objects.get(pk=1)
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(post_object.likes.get(username='testuser2').username, 'testuser2')

    def test_correct_like_content_data(self):
        self.client.login(username='testuser2', password='1234567')
        post_object = Post.objects.get(pk=1)
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "like_done"}', 'Wrong status or data!')

    def test_dislike_success(self):
        self.client.login(username='testuser2', password='1234567')
        post_object = Post.objects.get(pk=1)
        # Create "like"
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(post_object.likes.get(username='testuser2').username, 'testuser2')
        # ReCreate "like" = "dislike"
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(
            post_object.likes.all().exists(),
            # pk=2 belongs to the username: testuser2
            f"{CustomUser.objects.get(pk=2)}'s like has not been deleted"
        )

    def test_correct_dislike_content_data(self):
        self.client.login(username='testuser2', password='1234567')
        post_object = Post.objects.get(pk=1)
        # Create "like"
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "like_done"}', 'Wrong status or data!')
        # ReCreate "like" = "dislike"
        resp = self.client.post('/likes/', data={'object_slug': post_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "dislike_done"}', 'Wrong status or data!')


class TestInterestsList(TestCase):
    """TestInterestsList View Methods"""
    @classmethod
    def setUpTestData(cls):
        # Create 6 interests for tests
        number_of_interests = 6
        for interest_num in range(number_of_interests):
            Interest.objects.create(title='Title test text %s' % interest_num)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('interests_list_url'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/interests_list.html')

    def test_correct_objects_number(self):
        resp = self.client.get(reverse('interests_list_url'))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.context['object_list']), 6, "View returned the wrong number of objects per page!")


class TestInterestsDetail(TestCase):
    """TestInterestsDetail View Methods"""
    @classmethod
    def setUpTestData(cls):
        Interest.objects.create(title='Title test text')

    def test_view_uses_correct_template(self):
        interest = Interest.objects.get(pk=1)
        resp = self.client.get(reverse('interest_detail_url', args=[interest.slug]))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'common/interest_detail.html')
