from django.apps import AppConfig


class CommonApiConfig(AppConfig):
    name = 'common.common_api'
