from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from common.common_api.serializers import PostSerializer
from common.models import Post
from users.models import CustomUser


class PostListApiViewTests(APITestCase):
    """
    Test PostsListApiView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        Post.objects.create(body='text test', username=self.user1)
        self.client.logout()

    def test_pagination(self):
        number_of_posts = 11
        for post_num in range(number_of_posts):
            Post.objects.create(body='Body test text %s' % post_num, username=self.user1)
        response = self.client.get(reverse('common.common_api:post_list_api'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 3)
        self.assertFalse(response.data['links']['previous'])
        self.assertTrue(response.data['links']['next'])
        self.assertEqual(response.data['count'], 12)
        # The next page test
        response = self.client.get(reverse('common.common_api:post_list_api') + '?page=2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 3)
        self.assertTrue(response.data['links']['previous'])
        self.assertTrue(response.data['links']['next'])
        self.assertEqual(response.data['count'], 12)

    def test_fields_values(self):
        response = self.client.get(reverse('common.common_api:post_list_api'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Post.objects.count(), 1)
        self.assertEqual(response.json().get('results')[0]['id'], 1)
        self.assertEqual(response.json().get('results')[0]['body'], 'text test')

    def test_logged_in_create_post(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.post(reverse('common.common_api:post_list_api'), {'body': 'test body 2'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(Post.objects.get(body='test body 2'), 'An object with the passed field value was not created!')

    def test_not_logged_in_create_post(self):
        response = self.client.post(reverse('common.common_api:post_list_api'), {'body': 'test body'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        with self.assertRaises(Post.DoesNotExist):
            Post.objects.get(body='test body')

    def test_search_did_not_any_result(self):
        response = self.client.get(reverse('common.common_api:post_list_api') + '?search=Non-existent text')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('results'), [], 'This search should not return objects with any results.!')

    def test_search_success_result(self):
        response = self.client.get(reverse('common.common_api:post_list_api') + '?search=text test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('results')[0]['body'], 'text test')


class SinglePostViewTests(APITestCase):
    """
    Test API SinglePostView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.post1 = Post.objects.create(body='text test', username=self.user1)
        self.post1.save()
        self.client.logout()

    def test_not_logged_in_post_update(self):
        response = self.client.patch(
            reverse('common.common_api:post_single_api', args=[self.post1.slug]),
            {'body': 'Modified body test'}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertEqual(self.post1.body, 'text test', 'The field value has been changed!')

    def test_not_logged_in_post_delete(self):
        response = self.client.delete(reverse('common.common_api:post_single_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(Post.objects.get(slug=self.post1.slug), 'The object was deleted by an unauthorized user!')

    def test_logged_in_post_update_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.patch(
            reverse('common.common_api:post_single_api', args=[self.post1.slug]),
            {'body': 'Modified body test'}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['body'], 'Modified body test', 'The field value has not been changed!')

    def test_logged_in_post_delete_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.delete(reverse('common.common_api:post_single_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Post.DoesNotExist):
            Post.objects.get(pk=1)

    def test_retrieve_post_success(self):
        response = self.client.get(reverse('common.common_api:post_single_api', args=[self.post1.slug]))
        serializer_data = PostSerializer(Post.objects.get(slug=self.post1)).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(), serializer_data,
            'The queryset from the database and the received object from the request are not the same!'
        )


class LikesViewTests(APITestCase):
    """
    Test API LikesView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.post1 = Post.objects.create(body='text test', username=self.user1)
        self.post1.save()
        self.client.logout()

    def test_not_logged_in_like(self):
        response = self.client.put(reverse('common.common_api:likes_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertFalse(
            self.user1 in self.post1.likes.all(),
            'An unauthorized username should not be added to the likes field!'
        )

    def test_not_logged_in_delete_like(self):
        # Add like
        self.post1.likes.add(self.user1)
        # Delete like
        response = self.client.delete(reverse('common.common_api:likes_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            self.user1 in self.post1.likes.all(),
            'An unauthorized username should not be removed from the likes field!'
        )

    def test_logged_in_like_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.put(reverse('common.common_api:likes_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.user1 in self.post1.likes.all(), 'Username has not been added to the likes field!')

    def test_logged_in_delete_like_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        # Add like
        self.client.put(reverse('common.common_api:likes_api', args=[self.post1.slug]))
        # Delete like
        response = self.client.delete(reverse('common.common_api:likes_api', args=[self.post1.slug]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(self.user1 in self.post1.likes.all(), 'Username has not been removed from the likes field!')
