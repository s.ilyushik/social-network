from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAuthorOrReadOnly(BasePermission):
    """
    Custom User Permission

    User can do action if he object author otherwise read only.
    """
    safe_methods = SAFE_METHODS

    def has_object_permission(self, request, view, obj):

        if request.method in self.safe_methods:
            return True
        if hasattr(obj, 'username'):
            return obj.username == request.user
        if hasattr(obj, 'author'):
            return obj.author == request.user
        return False


class IsOwnerProfileOrReadOnly(BasePermission):
    """
    Custom User Permission

    User can do action if he object owner otherwise read only.
    """
    safe_methods = SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        if request.method in self.safe_methods:
            return True
        return obj == request.user
