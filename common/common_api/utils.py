from django_filters import rest_framework as filters
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from common.models import Post


class PostPagination(PageNumberPagination):
    """
    Pagination for Posts List
    """

    page_size = 3
    max_page_size = 10000

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data
        })


class PostFilter(filters.FilterSet):
    """
    Filter for Posts
    """

    username = filters.CharFilter(field_name='username__username', lookup_expr='icontains')
    interests = filters.CharFilter(field_name='interests__title', lookup_expr='icontains')
    year = filters.NumberFilter(field_name='date_time', lookup_expr='year')
    month = filters.NumberFilter(field_name='date_time', lookup_expr='month')

    class Meta:
        model = Post
        fields = ['username', 'date_time', 'interests']
