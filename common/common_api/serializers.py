from rest_framework import serializers

from common.models import Post, Interest
from comments.comments_api.serializers import CommentSerializer


class PostSerializer(serializers.ModelSerializer):
    """
    Serializer for Posts CRUD
    """
    image = serializers.ImageField(allow_null=True, required=False)
    username = serializers.StringRelatedField(read_only=True)
    slug = serializers.StringRelatedField(read_only=True)
    interests = serializers.SlugRelatedField(slug_field='title', many=True, queryset=Interest.objects.all())
    likes = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)
    comments = CommentSerializer(many=True, required=False)

    class Meta:
        model = Post
        fields = (
            'id',
            'body',
            'image',
            'username',
            'slug',
            'interests',
            'likes',
            'date_time',
            'comments',
        )


class LikesSerializer(serializers.ModelSerializer):
    """
    Serializer for Likes
    """
    slug = serializers.StringRelatedField(read_only=True)
    username = serializers.StringRelatedField(read_only=True)
    likes = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)

    class Meta:
        model = Post
        fields = (
            'id',
            'slug',
            'username',
            'likes',
        )
