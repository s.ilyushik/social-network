from django.urls import path
from .views import PostListApiView, SinglePostView, LikesView

app_name = "common.common_api"


urlpatterns = [
    path('posts_api/', PostListApiView.as_view(), name="post_list_api"),
    path('posts_api/<str:slug>/', SinglePostView.as_view(), name="post_single_api"),
    path('posts_api/likes/<str:slug>/', LikesView.as_view(), name="likes_api"),
]
