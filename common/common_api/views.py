from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework import permissions
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from .utils import PostFilter, PostPagination

from .serializers import PostSerializer, LikesSerializer
from .permissions import IsAuthorOrReadOnly
from common.models import Post


class PostListApiView(ListCreateAPIView):
    """
    All Posts View
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filterset_class = PostFilter
    pagination_class = PostPagination
    search_fields = ['body', 'interests__title', 'username__username', 'username__last_name']

    def perform_create(self, serializer):
        author = self.request.user
        return serializer.save(username=author)


class SinglePostView(RetrieveUpdateDestroyAPIView):
    """
    Retrieve Update Delete Single Posts View
    """
    queryset = Post.objects.all()
    lookup_field = 'slug'
    serializer_class = PostSerializer
    permission_classes = (IsAuthorOrReadOnly,)


class LikesView(RetrieveUpdateDestroyAPIView):
    """
    Add / Delete Like
    """
    queryset = Post.objects.all()
    lookup_field = 'slug'
    serializer_class = LikesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def put(self, request, *args, **kwargs):
        """
        Add Like
        """
        user_object = self.request.user
        post_object = self.get_object()
        post_object.likes.add(user_object)
        serializer = self.get_serializer(post_object)
        return Response(serializer.data, status=200)

    def delete(self, request, *args, **kwargs):
        """
        Remove Like
        """
        user_object = self.request.user
        post_object = self.get_object()
        post_object.likes.remove(user_object)
        return Response(status=204)
