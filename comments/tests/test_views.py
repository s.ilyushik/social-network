from django.test import TestCase
from django.urls import reverse

from users.models import CustomUser
from common.models import Post
from comments.models import Comment


class TestCommentCreate(TestCase):
    """Test CommentCreate View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='Test_username_1', email='1@1.com', password='1234567')
        test_user1.save()
        test_post1 = Post.objects.create(body='Test post body 1', username=test_user1)
        test_post1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        post = Post.objects.get(pk=1)
        user = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            reverse('comment_create_url', args=[post.slug]),
            {'post': post, 'body': 'lorem text 9', 'author': user}
        )
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/comments/create/{}/'.format(post.slug))
        self.assertFalse(post.comments.all().exists(), "The comment was created by mistake")

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='Test_username_1', password='1234567')
        post = Post.objects.get(pk=1)
        user = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            reverse('comment_create_url', args=[post.slug]),
            {'post': post, 'body': 'lorem text 9', 'author': user}
        )
        self.assertEqual(resp.status_code, 302)
        resp = self.client.get(reverse('comment_create_url', args=[post.slug]))
        self.assertTemplateUsed(resp, 'common/index.html', 'wrong template')

    def test_logged_in_redirect(self):
        self.client.login(username='Test_username_1', password='1234567')
        post = Post.objects.get(pk=1)
        user = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            reverse('comment_create_url', args=[post.slug]),
            {'post': post, 'body': 'lorem text 9', 'author': user}
        )
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/')

    def test_logged_in_object_create(self):
        self.client.login(username='Test_username_1', password='1234567')
        post = Post.objects.get(pk=1)
        user = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            reverse('comment_create_url', args=[post.slug]),
            {'post': post, 'body': 'lorem text 9', 'author': user}
        )
        self.assertEqual(resp.status_code, 302)
        self.assertTrue(post.comments.all().exists(), "No comment was created")

    def test_logged_in_required_data_missing(self):
        self.client.login(username='Test_username_1', password='1234567')
        post = Post.objects.get(pk=1)
        user = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            reverse('comment_create_url', args=[post.slug]),
            {'post': post, 'author': user}
        )
        self.assertEqual(
            resp.context['form'].errors['__all__'],
            ['You cannot create an empty comment (in which there is no image or text)!'],
            'The validator message does not match the one set in the form!'
        )


class TestCommentDelete(TestCase):
    """Test CommentDelete View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='Test_username_1', email='1@1.com', password='1234567')
        test_user1.save()
        test_post1 = Post.objects.create(body='Test post body 1', username=test_user1)
        test_post1.save()
        test_comment1 = Comment.objects.create(post=test_post1, body='lorem for delete', author=test_user1)
        test_comment1.save()
        self.client.logout()

    def test_logged_in_object_delete_redirect(self):
        self.client.login(username='Test_username_1', password='1234567')
        post = Post.objects.get(pk=1)
        resp = self.client.post('/comments/comment/' + post.comments.get(pk=1).slug + '/delete/')
        self.assertEqual(resp.status_code, 302)
        self.assertFalse(post.comments.all().exists(), "Comment has not been deleted")
        self.assertRedirects(resp, '/')

    def test_redirect_if_not_logged_in(self):
        post = Post.objects.get(pk=1)
        resp = self.client.post('/comments/comment/' + post.comments.get(pk=1).slug + '/delete/')
        self.assertEqual(resp.status_code, 302)
        self.assertTrue(post.comments.all().exists(), "The comment was deleted by mistake")
        self.assertRedirects(
            resp,
            f'/users/login/?next=/comments/comment/{post.comments.get(pk=1).slug}/delete/'
        )


class TestCommentsLikesDislikes(TestCase):
    """Test CommentsLikesDislikes View Methods"""

    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_user2 = CustomUser.objects.create_user(username='testuser2', email='222@22.com', password='1234567')
        test_user2.save()
        test_post1 = Post.objects.create(body='Body test', username=test_user1)
        test_post1.save()
        test_comment1 = Comment.objects.create(post=test_post1, body='lorem comment', author=test_user1)
        test_comment1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.post(reverse('comment_likes'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/comments/comments_likes/')

    def test_incorrect_slug_to_like_view(self):
        self.client.login(username='testuser2', password='1234567')
        with self.assertRaises(Comment.DoesNotExist):
            self.client.post('/comments/comments_likes/', data={'object_slug': 'my_does_not_exist_slug'})

    def test_like_success(self):
        self.client.login(username='testuser2', password='1234567')
        comment_object = Comment.objects.get(pk=1)
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(comment_object.likes.get(username='testuser2').username, 'testuser2', 'Like was not created')

    def test_correct_like_content_data(self):
        self.client.login(username='testuser2', password='1234567')
        comment_object = Comment.objects.get(pk=1)
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "like_done"}', 'Wrong status or data!')

    def test_dislike_success(self):
        self.client.login(username='testuser2', password='1234567')
        comment_object = Comment.objects.get(pk=1)
        # Create "like"
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(comment_object.likes.get(username='testuser2').username, 'testuser2')
        # ReCreate "like" = "dislike"
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(
            comment_object.likes.all().exists(),
            # pk=2 belongs to the username: testuser2
            f"{CustomUser.objects.get(pk=2)}'s like has not been deleted"
        )

    def test_correct_dislike_content_data(self):
        self.client.login(username='testuser2', password='1234567')
        comment_object = Comment.objects.get(pk=1)
        # Create "like"
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "like_done"}', 'Wrong status or data!')
        # ReCreate "like" = "dislike"
        resp = self.client.post('/comments/comments_likes/', data={'object_slug': comment_object.slug})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content.decode(), '{"status": "ok", "data": "dislike_done"}', 'Wrong status or data!')
