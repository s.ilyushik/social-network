from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework import permissions

from .serializers import CommentSerializer, CommentLikeSerializer
from comments.models import Comment
from common.common_api.permissions import IsAuthorOrReadOnly


class CreateCommentView(CreateAPIView):
    """
    Create Post's Comments
    """
    queryset = Comment.objects.all()
    lookup_field = 'slug'
    serializer_class = CommentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        author = self.request.user
        return serializer.save(author=author)


class SingleCommentView(RetrieveUpdateDestroyAPIView):
    """
    Retrieve Update Delete Single Comments View
    """
    queryset = Comment.objects.all()
    lookup_field = 'slug'
    serializer_class = CommentSerializer
    permission_classes = (IsAuthorOrReadOnly,)

    def perform_update(self, serializer):
        author = self.request.user
        return serializer.save(username=author)


class CommentsLikeView(RetrieveUpdateDestroyAPIView):
    """
    Add / Delete Like
    """
    queryset = Comment.objects.all()
    lookup_field = 'slug'
    serializer_class = CommentLikeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def put(self, request, *args, **kwargs):
        """
        Add Like
        """
        user_object = self.request.user
        comment_object = self.get_object()
        comment_object.likes.add(user_object)
        serializer = self.get_serializer(comment_object)
        return Response(serializer.data, status=200)

    def delete(self, request, *args, **kwargs):
        """
        Remove Like
        """
        user_object = self.request.user
        comment_object = self.get_object()
        comment_object.likes.remove(user_object)
        return Response(status=204)
