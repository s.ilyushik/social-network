from django.urls import path
from .views import SingleCommentView, CreateCommentView, CommentsLikeView

app_name = "comments.comments_api"


urlpatterns = [
    path('comments_api/<str:slug>/', SingleCommentView.as_view(), name="comment_single_api"),
    path('posts_api/<str:slug>/comments/', CreateCommentView.as_view(), name="create_comment_api"),
    path('comments_api/likes/<str:slug>/', CommentsLikeView.as_view(), name="comments_likes_api"),
]
