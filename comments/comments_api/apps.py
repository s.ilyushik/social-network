from django.apps import AppConfig


class CommentsApiConfig(AppConfig):
    name = 'comments.comments_api'
