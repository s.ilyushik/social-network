from rest_framework import serializers
from comments.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """
    Serializer for Comments
    """
    slug = serializers.StringRelatedField(read_only=True)
    author = serializers.StringRelatedField(read_only=True)
    likes = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)

    class Meta:
        model = Comment
        fields = (
            'id',
            'slug',
            'date_time',
            'body',
            'image',
            'post',
            'author',
            'likes',
        )


class CommentLikeSerializer(serializers.ModelSerializer):
    """
    Serializer for Likes
    """
    slug = serializers.StringRelatedField(read_only=True)
    author = serializers.StringRelatedField(read_only=True)
    likes = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)

    class Meta:
        model = Comment
        fields = (
            'id',
            'slug',
            'author',
            'likes',
        )
