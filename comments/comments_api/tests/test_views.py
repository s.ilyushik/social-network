from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from django.urls import reverse
from rest_framework import status

from comments.comments_api.serializers import CommentSerializer
from users.models import CustomUser
from common.models import Post
from comments.models import Comment


class CreateCommentViewTests(APITestCase):
    """
    Test CreateCommentView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.post1 = Post.objects.create(body='text test', username=self.user1)
        self.client.logout()

    def test_logged_in_create_comment(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.post(
            reverse('comments.comments_api:create_comment_api', args=[self.post1.slug]),
            {'body': 'test comment body 1', 'post': self.post1.pk}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            Comment.objects.get(body='test comment body 1'),
            'An object with the passed field value was not created!'
        )

    def test_not_logged_in_create_comment(self):
        response = self.client.post(
            reverse('comments.comments_api:create_comment_api', args=[self.post1.slug]),
            {'body': 'test comment body 1', 'post': self.post1.pk}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        with self.assertRaises(Comment.DoesNotExist):
            Comment.objects.get(body='test comment body 1')


class SingleCommentViewTests(APITestCase):
    """
    Test SingleCommentView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.post1 = Post.objects.create(body='text test', username=self.user1)
        self.post1.save()
        self.comment1 = Comment.objects.create(body='comment text test', author=self.user1, post=self.post1)
        self.post1.save()
        self.client.logout()

    def test_not_logged_in_comment_update(self):
        response = self.client.patch(
            reverse('comments.comments_api:comment_single_api', args=[self.comment1.slug]),
            {'body': 'Updated test comment body 1', 'post': self.post1.pk}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertEqual(self.comment1.body, 'comment text test', 'The field value has been changed!')

    def test_not_logged_in_comment_delete(self):
        response = self.client.delete(reverse('comments.comments_api:comment_single_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(Comment.objects.get(slug=self.comment1.slug), 'The object was deleted by an unauthorized user!')

    def test_logged_in_comment_update_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.patch(
            reverse('comments.comments_api:comment_single_api', args=[self.comment1.slug]),
            {'body': 'Updated test comment body 1', 'post': self.post1.pk}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json()['body'], 'Updated test comment body 1',
            'The field value has not been changed!'
        )

    def test_logged_in_comment_delete_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.delete(reverse('comments.comments_api:comment_single_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Comment.DoesNotExist):
            Comment.objects.get(pk=1)

    def test_retrieve_comment_success(self):
        response = self.client.get(
            reverse('comments.comments_api:comment_single_api', args=[self.comment1.slug]),
            {'post': self.post1.pk}
        )
        serializer_data = CommentSerializer(Comment.objects.get(slug=self.comment1.slug)).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.json(), serializer_data,
            'The queryset from the database and the received object from the request are not the same!'
        )


class CommentsLikeViewTests(APITestCase):
    """
    Test API CommentsLikeView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.post1 = Post.objects.create(body='text test', username=self.user1)
        self.post1.save()
        self.comment1 = Comment.objects.create(body='comment text test', author=self.user1, post=self.post1)
        self.post1.save()
        self.client.logout()

    def test_not_logged_in_like(self):
        response = self.client.put(reverse('comments.comments_api:comments_likes_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertFalse(
            self.user1 in self.comment1.likes.all(),
            'An unauthorized username should not be added to the likes field!'
        )

    def test_not_logged_in_delete_like(self):
        # Add like
        self.comment1.likes.add(self.user1)
        # Delete like
        response = self.client.delete(reverse('comments.comments_api:comments_likes_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            self.user1 in self.comment1.likes.all(),
            'An unauthorized username should not be removed from the likes field!'
        )

    def test_logged_in_like_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.put(reverse('comments.comments_api:comments_likes_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.user1 in self.comment1.likes.all(), 'Username has not been added to the likes field!')

    def test_logged_in_delete_like_success(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        # Add like
        self.client.put(reverse('comments.comments_api:comments_likes_api', args=[self.comment1.slug]))
        # Delete like
        response = self.client.delete(reverse('comments.comments_api:comments_likes_api', args=[self.comment1.slug]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(self.user1 in self.comment1.likes.all(), 'Username has not been removed from the likes field!')
