from django import forms
from django.core.exceptions import ValidationError

from .models import Comment


class CommentForm(forms.ModelForm):
    """
    Post comment form
    """

    class Meta:
        model = Comment
        fields = ['body', 'image']

        widgets = {
            'body': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': '3',
                'placeholder': 'Write your comment here ...',
                'cols': 50,
                'label': ''
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'custom_hide',
                'label': ''
            }),
        }

    def clean(self):
        cleaned_data = super(CommentForm, self).clean()
        body = self.cleaned_data.get('body')
        image = self.cleaned_data.get('image')
        if body == '' and not image:
            raise ValidationError('You cannot create an empty comment (in which there is no image or text)!')
        return cleaned_data