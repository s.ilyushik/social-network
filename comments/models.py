from django.db import models
from django.shortcuts import reverse
from settings.base import AUTH_USER_MODEL

from common.models import Post
from common.utils import slug_generator


class Comment(models.Model):
    """Post Comment Model"""
    slug = models.SlugField(max_length=150, unique=True)
    post = models.ForeignKey('common.Post', on_delete=models.CASCADE, related_name='comments')
    date_time = models.DateTimeField(auto_now_add=True)
    body = models.TextField(blank=True, verbose_name="Comment_text")
    image = models.ImageField(
        upload_to='users_uploads/comments_images',
        blank=True,
        verbose_name="Upload a picture for your comment"
    )
    author = models.ForeignKey(
        AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name='Comment author',
        related_name='comments'
    )
    likes = models.ManyToManyField(AUTH_USER_MODEL, blank=True, verbose_name='Liker', related_name='commens_was_liked')

    def get_update_url(self):
        return reverse('comment_update_url', kwargs={'slug': self.slug})

    def __str__(self):
        return '{}'.format(self.body)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = 'comment_' + slug_generator(self.body[:10], self.author)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ('-date_time',)
