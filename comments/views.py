from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from django.core.exceptions import ObjectDoesNotExist

from .models import Comment
from .forms import CommentForm
from common.models import Post
from django.contrib.auth import get_user_model


class CommentCreate(LoginRequiredMixin, CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'common/index.html'
    success_url = reverse_lazy('posts_list_url')
    login_url = 'login'

    def form_valid(self, bound_comment_form):
        post_slug = self.kwargs['slug']
        bound_comment_form.instance.post = Post.objects.get(slug=post_slug)
        bound_comment_form.instance.author = self.request.user
        bound_comment_form.save()
        return super().form_valid(bound_comment_form)


class CommentDelete(LoginRequiredMixin, DeleteView):
    model = Comment
    template_name = 'common/index.html'
    success_url = reverse_lazy('posts_list_url')
    login_url = 'login'


class CommentsLikesDislikes(LoginRequiredMixin, UpdateView):
    """
    Comments's like button and dislike button.

    Inverting logic and appearance after clicking.
    """
    model = Comment
    login_url = 'login'

    def _like(self, request):
        try:
            user_pk, comment_object = self.get_objects_and_args(request)
            comment_object.likes.add(get_user_model().objects.get(pk=user_pk))
            message = 'like_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _dislike(self, request):
        try:
            user_pk, comment_object = self.get_objects_and_args(request)
            comment_object.likes.remove(get_user_model().objects.get(pk=user_pk))
            message = 'dislike_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    # Retrieving values for use in the methods of this view
    def get_objects_and_args(self, request):
        user_pk = self.request.user.pk
        comment_object = Comment.objects.get(slug__iexact=request.POST.get('object_slug'))
        return user_pk, comment_object

    @method_decorator(require_POST)
    def post(self, request, *args, **kwargs):
        user_pk, comment_object = self.get_objects_and_args(request)

        if get_user_model().objects.get(pk=user_pk) not in comment_object.likes.all():
            return self._like(request)

        if get_user_model().objects.get(pk=user_pk) in comment_object.likes.all():
            return self._dislike(request)
        return JsonResponse({'status': 'error', 'reason': 'Bad request'}, status=400)
