// Previewer of comment img upload
function imgUploadPreviewer(elementId) {
    var input_block = $('span#' + elementId).children();
    var imgNameType = input_block[0].files[0].name;

    if (input_block[0].files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(".wasAddMes" + showInDiv).attr('src', e.target.result).addClass('imgPreviewBlock');
        };
        reader.readAsDataURL(input_block[0].files[0]);
    }

    var showInDiv = elementId + 'msg';
    $(".wasAddMes" + showInDiv).parent().next().text('You have attached: ' + imgNameType);

    // Generate an id
    // The first letters are removed from the id to leave only the slug (the original id was kind: cnslprv{{ post.slug }})
    var spanElement = elementId.slice(2);
    var deleteButtonElement = 'cnslprv' + spanElement;

    $('#' + deleteButtonElement).removeClass('custom_hide');
}


function cancelImgUploadPreviewer(elementId) {
    // Generate select (class id) for delete button element
    var buttonElement = $('.cancelPreview#' + elementId);

    // Generate id of input's span (spanElementId)
    // The first letters are removed from the id to leave only the slug (the original id was kind: cnslprv{{ post.slug }})
    var inputElement = elementId.slice(7);
    var spanElementId = 'u_' + inputElement;
    var input_block = $('span#'+ spanElementId).children();
    var img = input_block[0].files[0];

    // Cleaning input
    input_block[0].value = '';

    // Hide the delete button after cancel the image
    buttonElement.toggleClass('custom_hide');

    // Delete img and text from DOM
    buttonElement.prev().prev().children().toggleClass("imgPreviewBlock").attr('src', '');
    buttonElement.prev().text('');
}