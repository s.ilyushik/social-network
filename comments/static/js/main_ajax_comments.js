$(document).ready(function () {


    // Call modal window for comment delete confirmation
    $(".comment_delete_btn").click(function() {
        $(".modal_comment_delete_confirmation").modal('show');
    });


    // Ajax comment delete
    $(".delete_comment_confirmation_btn").on("click", function (e) {

        e.preventDefault();
        var slug = $(this).attr('data-slug');
        var post_slug = $(this).attr('data-post-slug');

        $.ajax({
            method: "POST",
            url: window.location.origin + '/comments/comment/' + slug + '/delete/',
            data: slug,
            success: function () {
               $(".comment" + slug).remove();
               $('.comments_count_' + post_slug).html(Number($('.comments_count_' + post_slug).html()) - 1);
            },
            error: function (data) {console.log(data);}
        });
    });
});