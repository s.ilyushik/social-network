$(document).ready(function () {


    // Like / Dislike functionality
    $('.like_link').on("click", function (e) {
        e.preventDefault();
        var object_slug = $(this).attr('data-slug');
        // To use a fixed value in $.ajax
        var link = $(this);

        $.ajax({
            method: "POST",
            url: window.location.origin + '/comments/comments_likes/',
            data: {object_slug: object_slug},
            success: function(data){
                if (data.data === 'like_done') {
                    // Increase the counter of likes after pressing the link
                    $('.comm_like_counter_' + object_slug).html(Number($('.comm_like_counter_' + object_slug).html()) + 1);
                    $('.comm_like_text_link_' + object_slug).html("I don't like it!");
                }
                if (data.data === 'dislike_done') {
                    // Decrease the counter of likes after pressing the link
                    $('.comm_like_counter_' + object_slug).html(Number($('.comm_like_counter_' + object_slug).html()) - 1);
                    $('.comm_like_text_link_' + object_slug).html("I like it!");
                }
                },
            error: function (data) {console.log(data);}
        });
    })
});