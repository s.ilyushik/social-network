from django.urls import path
from comments.views import CommentCreate, CommentDelete, CommentsLikesDislikes


urlpatterns = [
    path('create/<str:slug>/', CommentCreate.as_view(), name ="comment_create_url"),
    path('comment/<str:slug>/delete/', CommentDelete.as_view(), name="comment_delete_url"),
    path('comments_likes/', CommentsLikesDislikes.as_view(), name="comment_likes"),
]
