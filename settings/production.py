from settings.base import *

DEBUG = False

ALLOWED_HOSTS = os.getenv("STAGE_ALLOWED_HOST").split(" ")

DATABASES = {
    'default': {
        'ENGINE': os.getenv("STAGE_DB_ENGINE"),
        'NAME': os.getenv("STAGE_DB_NAME"),
        'USER': os.getenv("STAGE_DB_USER"),
        'PASSWORD': os.getenv("STAGE_DB_PASSWORD"),
        'HOST': os.getenv("STAGE_DB_HOST"),
        'PORT': os.getenv("STAGE_DB_PORT"),
    }
}

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
