$(document).ready(function () {


    // Manage friendship requests, add and remove from friends, change the button with this functionality
    $(".friends_btn").on("click", function (e) {

        e.preventDefault();
        var friendship_request_recipient_id = $(this).attr('data-friendship_request_recipient_id');
        var friendship_request_recipient_profile_url = $(this).attr('data-friendship_request_recipient_profile_url');
        var user_action = $(this).attr('data-user_action');
        // To use a fixed value in $.ajax
        var button = $(this);

        $.ajax({
            method: "POST",
            url: friendship_request_recipient_profile_url + 'friendship/',
            data: {friendship_request_recipient_id:friendship_request_recipient_id, user_action:user_action},
            success: function (data) {
                if (data.data === 'friendship_request_done') {
                    button.removeClass('btn-primary').addClass('btn-light').attr('data-user_action','friendship_request_cancel').removeAttr('onclick').text('Cancel Friend Request');
                }
                if (data.data === 'friendship_request_cancel_done') {
                    button.removeClass('btn-light').addClass('btn-primary').attr('data-user_action','friendship_request').removeAttr('onclick').text('+Add to Friends');
                }
                if (data.data === 'friendship_confirmation_done') {
                    button.removeClass('btn-outline-success').addClass('btn-info').attr('data-user_action','break_friendship').removeAttr('onclick').text('Your Friend');
                }
                if (data.data === 'break_friendship_done' && !button.hasClass("delete_friend_independent")) {
                    button.removeClass('btn-info').addClass('btn-primary').attr('data-user_action','friendship_request').removeAttr('onclick').text('+Add to Friends');
                }
                if (button.hasClass("delete_friend_independent")){
                    button.parent().remove('.friends_row');
                }

                if (button.hasClass("delete_friend_independent_modal")){
                    button.parent().remove();
                }
                if (button.hasClass("add_friend_independent")){
                    $('.reload_friends').children().remove('.friends_row');
                    $('.reload_friends').load(' .friends_row');
                    button.parent().parent().parent().remove('.friendship_offers_row');
                    $('.reload_friends_modal').children().remove('.friends_row_modal');
                    $('.reload_friends_modal').parent().parent().load(' .friends_row_modal');
                }
                if (data.data === 'reject_friendship_offer_done') {
                    button.parent().parent().parent().remove('.friendship_offers_row');
                }
                },
            error: function (data) {console.log(data);}
        });
    });
});