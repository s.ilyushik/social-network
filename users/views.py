from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DetailView
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from django.shortcuts import redirect
from django.contrib import messages

from .forms import CustomUserCreationForm, CustomUserChangeForm, InterestForm
from comments.forms import CommentForm
from .models import CustomUser
from common.models import Post


class SignUp(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class ProfileDetail(DetailView):
    model = CustomUser
    template_name = 'users_profile.html'

    def get_context_data(self, **kwargs):
        form_of_comment = CommentForm(auto_id=False)
        context = super().get_context_data(**kwargs)
        context['form_of_comment'] = form_of_comment
        # Get the current user's posts
        context['posts'] = Post.objects.filter(username__exact=self.get_object().pk)
        return context


class OwnProfileUpdate(UserPassesTestMixin, UpdateView):
    """
    Updating user profile
    """
    def test_func(self):
        return self.request.user.pk == self.get_object().pk

    model = CustomUser
    form_class = CustomUserChangeForm
    template_name = 'user_profile_update.html'
    login_url = 'login'


class FriendshipManagement(LoginRequiredMixin, UpdateView):
    """
    Friendship Management View

    Sending and accepting a friendship request. Friendship cancellation.
    Depending on the response of this view (the variable "message")
    in the template, the appearance and logic of the friendship button changes.
    """
    model = CustomUser
    login_url = 'login'

    def _friendship_request(self, request):
        user_pk, user_object, user_for_friendship_pk, user_for_friendship_object = self.get_users_obj(request)

        try:
            user_for_friendship_object.friendship_offers.add(CustomUser.objects.get(pk=user_pk))
            message = 'friendship_request_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _friendship_request_cancel(self, request):
        user_pk, user_object, user_for_friendship_pk, user_for_friendship_object = self.get_users_obj(request)

        try:
            user_for_friendship_object.friendship_offers.remove(CustomUser.objects.get(pk=user_pk))
            message = 'friendship_request_cancel_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _friendship_confirmation(self, request):
        user_pk, user_object, user_for_friendship_pk, user_for_friendship_object = self.get_users_obj(request)

        try:
            user_object.friendship_offers.remove(CustomUser.objects.get(pk=user_for_friendship_pk))
            user_object.friends.add(CustomUser.objects.get(pk=user_for_friendship_pk))
            user_for_friendship_object.friends.add(CustomUser.objects.get(pk=user_pk))
            message = 'friendship_confirmation_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _break_friendship(self, request):
        user_pk, user_object, user_for_friendship_pk, user_for_friendship_object = self.get_users_obj(request)

        try:
            user_object.friends.remove(CustomUser.objects.get(pk=user_for_friendship_pk))
            user_for_friendship_object.friends.remove(CustomUser.objects.get(pk=user_pk))
            message = 'break_friendship_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def _reject_friendship_offer(self, request):
        user_pk, user_object, user_for_friendship_pk, user_for_friendship_object = self.get_users_obj(request)

        try:
            user_object.friendship_offers.remove(CustomUser.objects.get(pk=user_for_friendship_pk))
            message = 'reject_friendship_offer_done'
            return JsonResponse({'status': 'ok', 'data': message})
        except ObjectDoesNotExist:
            return JsonResponse({'status': 'error', 'reason': 'User not found'}, status=404)

    def get_users_obj(self, request):
        """
        Retrieving values for use in the methods of this view
        """
        user_pk = self.request.user.pk
        user_object = CustomUser.objects.get(pk=user_pk)
        user_for_friendship_pk = request.POST.get('friendship_request_recipient_id')
        user_for_friendship_object = CustomUser.objects.get(pk=user_for_friendship_pk)
        return user_pk, user_object, user_for_friendship_pk, user_for_friendship_object

    @method_decorator(require_POST)
    def post(self, request, *args, **kwargs):
        user_action = request.POST.get('user_action')

        if user_action == 'friendship_request':
            return self._friendship_request(request)

        if user_action == 'friendship_request_cancel':
            return self._friendship_request_cancel(request)

        if user_action == 'friendship_confirmation':
            return self._friendship_confirmation(request)

        if user_action == 'break_friendship':
            return self._break_friendship(request)

        if user_action == 'reject_friendship_offer':
            return self._reject_friendship_offer(request)
        return JsonResponse({'status': 'error', 'reason': 'Bad request'}, status=400)


class AddOrRemoveInterestsInProfile(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """
    Interest adding view

    A view that allows you to add interests from the Interest list to the user's interests field.
    """
    model = CustomUser
    form_class = InterestForm
    template_name = 'common/index.html'
    success_url = reverse_lazy('posts_list_url')
    login_url = 'login'

    def test_func(self):
        return self.request.user.pk == self.get_object().pk

    def _add_new_interests(self, request):
        """
        Adds only new interests to the user's list of interests
        """
        user_object, interests_form_pk_list = self.get_values(request)
        bound_form = self.form_class(request.POST, instance=user_object)
        if interests_form_pk_list == []:
            messages.success(request, 'You must select at least one item to add!')
        if bound_form.is_valid():
            interests_form_pk_list = [
                i for i in interests_form_pk_list if not user_object.user_interests.filter(pk=i)
            ]
            user_object.user_interests.add(*interests_form_pk_list)
        return redirect('posts_list_url')

    def _remove_any_interests(self, request):
        """
        Removing interests if they are in the user’s list
        """
        user_object, interests_form_pk_list = self.get_values(request)
        bound_form = self.form_class(request.POST, instance=user_object)
        if interests_form_pk_list == []:
            messages.success(request, 'You must select at least one item to delete!')
        if bound_form.is_valid():
            user_object.user_interests.remove(*interests_form_pk_list)
        return redirect('posts_list_url')

    def get_values(self, request):
        """
        Retrieving values for use in the methods of this view
        """
        user_pk = self.request.user.pk
        user_object = CustomUser.objects.get(pk=user_pk)
        interests_form_pk_list = self.request.POST.getlist('user_interests')
        return user_object, interests_form_pk_list

    def post(self, request, *args, **kwargs):
        if 'delete_interest' in self.request.POST:
            return self._remove_any_interests(request)
        elif 'add_interest' in self.request.POST:
            return self._add_new_interests(request)
        return redirect('posts_list_url')
