from django.urls import path
from .views import (
    FriendshipRequestView,
    FriendshipConfirmationView,
    FriendshipBreakView,
    UserInterestUpdateView,
    UsersSearchListView,
)

app_name = "users.users_api"


urlpatterns = [
    path('users_api/user_search_list/', UsersSearchListView.as_view(), name="user_search_list"),
    path('users_api/friendship_request/<int:pk>/', FriendshipRequestView.as_view(), name="friendship_request"),
    path(
        'users_api/friendship_confirmation/<int:pk>/',
        FriendshipConfirmationView.as_view(),
        name="friendship_confirmation"
    ),
    path('users_api/friendship_break/<int:pk>/', FriendshipBreakView.as_view(), name="friendship_break"),
    path('users_api/user_interest/<int:pk>/', UserInterestUpdateView.as_view(), name="user_interest"),
]
