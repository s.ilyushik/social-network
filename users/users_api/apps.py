from django.apps import AppConfig


class UsersApiConfig(AppConfig):
    name = 'users.users_api'
