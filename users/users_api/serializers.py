from rest_framework import serializers

from users.models import CustomUser
from common.models import Interest


class UserSerializerPublic(serializers.ModelSerializer):
    """
    Main Public Custom User Serializer

    Custom user serializer without excluded private fields.
    """
    status_user = serializers.CharField(max_length=100, allow_blank=True)
    about_user = serializers.CharField(max_length=500, allow_blank=True)
    user_interests = serializers.SlugRelatedField(slug_field='title', many=True, queryset=Interest.objects.all())
    friends = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)
    avatar = serializers.ImageField(allow_null=True)
    profile_bg_image = serializers.ImageField(allow_null=True)

    class Meta:
        model = CustomUser
        ref_name = "Main user serializer"
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'avatar',
            'profile_bg_image',
            'status_user',
            'about_user',
            'user_interests',
            'friends',
        )


class FriendshipSerializer(serializers.ModelSerializer):
    """
    Friendship Serializer
    """
    friendship_offers = serializers.SlugRelatedField(slug_field='username', many=True, queryset=CustomUser.objects.all())
    friends = serializers.SlugRelatedField(slug_field='username', many=True, read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'friendship_offers',
            'friends',
        )


class UserInterestsSerializer(serializers.ModelSerializer):
    """
    User Interest Serializer
    """
    user_interests = serializers.SlugRelatedField(slug_field='title', many=True, queryset=CustomUser.objects.all())

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'username',
            'user_interests',
        )
