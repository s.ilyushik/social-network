from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from django.urls import reverse
from rest_framework import status

from users.models import CustomUser
from common.models import Interest
from users.users_api.serializers import UserInterestsSerializer, UserSerializerPublic


class CustomTokenObtainPairViewTests(APITestCase):
    """
    Test CustomTokenObtainPairView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.client.logout()

    def test_correct_pair_getting_token(self):
        # Tests with username password pair
        response = self.client.post('/auth/jwt/create/', {'username': 'test_user1', 'password': '1234567'})
        token_access = response.data['access']
        token_refresh = response.data['refresh']
        response = self.client.post('/auth/jwt/verify/', {'token': token_access})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post('/auth/jwt/verify/', {'token': token_refresh})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post('/auth/jwt/verify/', {'token': 'wrong token'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'Token is invalid or expired')
        # Tests with email password pair
        response = self.client.post('/auth/jwt/create/', {'email': '1@1.com', 'password': '1234567'})
        token_access = response.data['access']
        token_refresh = response.data['refresh']
        response = self.client.post('/auth/jwt/verify/', {'token': token_access})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post('/auth/jwt/verify/', {'token': token_refresh})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post('/auth/jwt/verify/', {'token': 'wrong token'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'Token is invalid or expired')

    def test_incorrect_pair_getting_token(self):
        # Wrong username
        response = self.client.post('/auth/jwt/create/', {'username': 'wrong', 'password': '1234567'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'No active account found with the given credentials')
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['access'])
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['refresh'])
        # Wrong email
        response = self.client.post('/auth/jwt/create/', {'email': 'wrong email', 'password': '1234567'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'No active account found with the given credentials')
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['access'])
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['refresh'])
        # Wrong password
        response = self.client.post('/auth/jwt/create/', {'username': 'test_user1', 'password': 'wrong password'})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data['detail'], 'No active account found with the given credentials')
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['access'])
        with self.assertRaises(KeyError):
            self.assertFalse(response.data['refresh'])
        # Full username or email
        response = self.client.post('/auth/jwt/create/', {'password': 'wrong password'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, 'Either name or email is required')


class FriendshipRequestViewTests(APITestCase):
    """
    Test FriendshipRequestView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.user2 = CustomUser.objects.create_user(username='test_user2', email='2@2.com', password='1234567')
        self.user2.save()
        self.user2_token = Token.objects.create(user=self.user2)
        self.client.logout()

    def test_successful_request_by_authorized_user(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        # Sending the pc of a future friend
        response = self.client.post(reverse('users.users_api:friendship_request', args=[self.user2.pk]))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            self.user1 in self.user2.friendship_offers.all(),
            'Username was not specified in the friendship_offers field!'
        )

    def test_unsuccessful_request_by_unauthorized_user(self):
        user_for_request = CustomUser.objects.get(pk=2)
        response = self.client.post(reverse('users.users_api:friendship_request', args=[user_for_request.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertFalse(
            self.user1 in self.user2.friendship_offers.all(),
            'Username was specified in the friendship_offers field by mistake!'
        )

    def test_unsuccessful_request_by_authorized_user_wrong_pk(self):
        wrong_user_for_request_pk = 3
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        with self.assertRaises(CustomUser.DoesNotExist):
            response = self.client.post(reverse('users.users_api:friendship_request', args=[wrong_user_for_request_pk]))

    def test_successful_cancel_request_by_authorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.delete(reverse('users.users_api:friendship_request', args=[self.user2.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            self.user1 in self.user2.friendship_offers.all(),
            'Username has not been removed from the friendship request field!'
        )

    def test_unsuccessful_cancel_request_by_unauthorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        response = self.client.delete(reverse('users.users_api:friendship_request', args=[self.user2.pk]))
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertTrue(
            self.user1 in self.user2.friendship_offers.all(),
            'Username has not been removed from the friendship request field!'
        )


class FriendshipConfirmationViewTest(APITestCase):
    """
    Test FriendshipConfirmationView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.user2 = CustomUser.objects.create_user(username='test_user2', email='2@2.com', password='1234567')
        self.user2.save()
        self.user2_token = Token.objects.create(user=self.user2)
        self.client.logout()

    def test_successful_friendship_confirmation_by_authorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user2_token.key)
        response = self.client.patch(reverse('users.users_api:friendship_confirmation', args=[self.user1.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(
            self.user1 in self.user2.friendship_offers.all(),
            'Username has not been removed from the friendship request field!'
        )
        self.assertTrue(self.user1 in self.user2.friends.all(), 'Username has not been added to the friends field!')
        self.assertTrue(self.user2 in self.user1.friends.all(), 'Username has not been added to the friends field!')

    def test_unsuccessful_friendship_confirmation_by_unauthorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        response = self.client.patch(reverse('users.users_api:friendship_confirmation', args=[self.user1.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            self.user1 in self.user2.friendship_offers.all(),
            'Username was removed from the friendship request field!'
        )
        self.assertFalse(self.user1 in self.user2.friends.all(), 'Username was added to the friends field!')
        self.assertFalse(self.user2 in self.user1.friends.all(), 'Username was added to the friends field!')

    def test_failed_confirmation_without_prior_friendship_offer(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.patch(reverse('users.users_api:friendship_confirmation', args=[self.user2.pk]))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), 'Missing application from your new friend')

    def test_successful_friendship_offer_decline_by_authorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user2_token.key)
        response = self.client.delete(reverse('users.users_api:friendship_confirmation', args=[self.user1.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            self.user1 in self.user2.friendship_offers.all(),
            'Username has not been removed from the friendship request field!'
        )

    def test_unsuccessful_friendship_offer_decline_by_unauthorized_user(self):
        # Make friendship offer for user2
        self.user2.friendship_offers.add(self.user1)
        response = self.client.delete(reverse('users.users_api:friendship_confirmation', args=[self.user1.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            self.user1 in self.user2.friendship_offers.all(),
            'Username was removed from the friendship request field!'
        )


class FriendshipBreakViewTests(APITestCase):
    """
    Test FriendshipBreakView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        self.user2 = CustomUser.objects.create_user(username='test_user2', email='2@2.com', password='1234567')
        self.user2.save()
        self.user2_token = Token.objects.create(user=self.user2)
        self.client.logout()

    def test_successful_friendship_break_by_authorized_user(self):
        # Make friendship users
        self.user1.friends.add(self.user2)
        self.user2.friends.add(self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.delete(reverse('users.users_api:friendship_break', args=[self.user2.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            self.user1 in self.user2.friends.all(),
            'Username has not been removed from the friends field!'
        )
        self.assertFalse(
            self.user2 in self.user1.friends.all(),
            'Username has not been removed from the friends field!'
        )

    def test_unsuccessful_friendship_break_by_unauthorized_user(self):
        # Make friendship users
        self.user1.friends.add(self.user2)
        self.user2.friends.add(self.user1)
        response = self.client.delete(reverse('users.users_api:friendship_break', args=[self.user2.pk]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            self.user1 in self.user2.friends.all(),
            'Username was removed from the friends field!'
        )
        self.assertTrue(
            self.user2 in self.user1.friends.all(),
            'Username was removed from the friends field!'
        )


class UserInterestUpdateViewTests(APITestCase):
    """
    Test UserInterestUpdateView Methods
    """
    def setUp(self):
        self.user1 = CustomUser.objects.create_user(username='test_user1', email='1@1.com', password='1234567')
        self.user1.save()
        self.user1_token = Token.objects.create(user=self.user1)
        # Create 6 interests for tests
        number_of_interests = 6
        for interest_num in range(number_of_interests):
            Interest.objects.create(title='Title test text %s' % interest_num)
        self.client.logout()

    def test_successfully_adding_interests_authorized_user(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        response = self.client.put(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        serializer_data = UserInterestsSerializer(CustomUser.objects.get(pk=1)).data
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer_data)

    def test_unsuccessfully_adding_interests_unauthorized_user(self):
        response = self.client.put(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        serializer_data = UserInterestsSerializer(CustomUser.objects.get(pk=1)).data
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')

    def test_unsuccessfully_adding_interests_wrong_user_pk(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        wrong_user_pk = '2'
        response = self.client.put(
            reverse('users.users_api:user_interest', args=[wrong_user_pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        serializer_data = UserInterestsSerializer(CustomUser.objects.get(pk=1)).data
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json()['detail'], 'Not found.')

    def test_successfully_removed_interests_authorized_user(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        # Adding some interests
        self.client.put(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        # Removing some interests
        response = self.client.delete(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [2, 3]},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(
            Interest.objects.get(pk=1) in self.user1.user_interests.all(),
            'Interest was removed from the user_interests field!'
        )
        self.assertFalse(
            Interest.objects.get(pk=2) in self.user1.user_interests.all(),
            'Interests has not been removed from the user_interests field!'
        )
        self.assertFalse(
            Interest.objects.get(pk=3) in self.user1.user_interests.all(),
            'Interests has not been removed from the user_interests field!'
        )

    def test_unsuccessfully_removed_interests_unauthorized_user(self):
        # Adding some interests
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        self.client.put(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        self.client.logout()
        # Removing some interests
        response = self.client.delete(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [2, 3]},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json()['detail'], 'Authentication credentials were not provided.')
        self.assertTrue(
            Interest.objects.get(pk=1) in self.user1.user_interests.all(),
            'Interest was removed from the user_interests field!'
        )
        self.assertTrue(
            Interest.objects.get(pk=2) in self.user1.user_interests.all(),
            'Interests has not been removed from the user_interests field!'
        )
        self.assertTrue(
            Interest.objects.get(pk=3) in self.user1.user_interests.all(),
            'Interests has not been removed from the user_interests field!'
        )

    def test_unsuccessfully_removed_interests_wrong_user_pk(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.user1_token.key)
        # Adding some interests
        self.client.put(
            reverse('users.users_api:user_interest', args=[self.user1.pk]),
            {"interests": [1, 2, 3]},
            format='json'
        )
        # Removing some interests
        wrong_user_pk = 2
        response = self.client.delete(
            reverse('users.users_api:user_interest', args=[wrong_user_pk]),
            {"interests": [2, 3]},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json()['detail'], 'Not found.')


class UsersSearchListViewTests(APITestCase):
    """
    Test UsersSearchListView Methods
    """
    def setUp(self):
        self.interest1 = Interest.objects.create(title='test interest')
        self.interest1.save()
        self.user1 = CustomUser.objects.create_user(
            username='test_user1',
            last_name='test_last_name',
            status_user='test_status_user',
            about_user='test_about_user',
            email='1@1.com',
            password='1234567'
        )
        self.user1.save()
        self.user1.user_interests.add(self.interest1)
        self.user2 = CustomUser.objects.create_user(username='test_user2', email='2@2.com', password='1234567')
        self.user2.save()

    def test_search_results_by_username(self):
        response = self.client.get('/api/users_api/user_search_list/?search=test_user1', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = UserSerializerPublic(CustomUser.objects.get(username='test_user1')).data
        self.assertEqual(
            response.json().get('results'),
            [serializer_data],
            'The search data is not identical to the database object!'
        )

    def test_search_results_by_last_name(self):
        response = self.client.get('/api/users_api/user_search_list/?search=test_last_name', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = UserSerializerPublic(CustomUser.objects.get(last_name='test_last_name')).data
        self.assertEqual(
            response.json().get('results'),
            [serializer_data],
            'The search data is not identical to the database object!'
        )

    def test_search_results_by_status_user(self):
        response = self.client.get('/api/users_api/user_search_list/?search=test_status_user', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = UserSerializerPublic(CustomUser.objects.get(status_user='test_status_user')).data
        self.assertEqual(
            response.json().get('results'),
            [serializer_data],
            'The search data is not identical to the database object!'
        )

    def test_search_results_by_about_user(self):
        response = self.client.get('/api/users_api/user_search_list/?search=test_about_user', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = UserSerializerPublic(CustomUser.objects.get(about_user='test_about_user')).data
        self.assertEqual(
            response.json().get('results'),
            [serializer_data],
            'The search data is not identical to the database object!'
        )

    def test_search_results_by_user_interests(self):
        response = self.client.get('/api/users_api/user_search_list/?search=test interest', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        serializer_data = UserSerializerPublic(CustomUser.objects.get(user_interests__title='test interest')).data
        self.assertEqual(
            response.json().get('results'),
            [serializer_data],
            'The search data is not identical to the database object!'
        )

    def test_search_results_by_empty_search_query(self):
        response = self.client.get("/api/users_api/user_search_list/?search=", format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['count'],
            CustomUser.objects.count(),
            'The number of objects for an empty query must match the total number of objects in the database!'
        )
