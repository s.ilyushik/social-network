from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework import permissions
from rest_framework import filters

from common.common_api import permissions as custom_permissions
from users.users_api.serializers import FriendshipSerializer, UserInterestsSerializer, UserSerializerPublic
from users.models import AbstractUser, CustomUser


class CustomTokenObtainPairView(TokenObtainPairView):
    """
    Custom Token Obtain Pair View.

    View provides a pair of tokens in response to username - password or email - password.
    """
    serializer_class = TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        if 'username' in self.request.data:
            # Change default value in authentication field in base User model
            AbstractUser.USERNAME_FIELD = 'username'
        elif 'email' in self.request.data:
            AbstractUser.USERNAME_FIELD = 'email'
        else:
            # If both username and email fields are not filled
            return Response('Either name or email is required', status=400)
        # Assign authentication field for custom user model
        TokenObtainPairSerializer.username_field = CustomUser.USERNAME_FIELD
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        tokens = serializer.validated_data
        return Response(tokens, status=201)


class FriendshipRequestView(APIView):
    """
    Friendship Request / Cancel
    """
    queryset = CustomUser.objects.all()
    serializer_class = FriendshipSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def post(self, request, *args, **kwargs):
        """
        Friendship Request
        """
        username_pk = self.request.user.pk
        friend_pk = kwargs.get('pk')
        username_pk_object = CustomUser.objects.get(pk=username_pk)
        friend_pk_object = CustomUser.objects.get(pk=friend_pk)
        friend_pk_object.friendship_offers.add(username_pk_object)
        serializer = self.serializer_class(friend_pk_object)
        # Dictionary with excluding data about friend requests
        public_data = serializer.data
        del public_data['friendship_offers']
        return Response(public_data, status=201)

    def delete(self, request, *args, **kwargs):
        """
        Friendship Request Cancel
        """
        username_pk = self.request.user.pk
        friend_pk = kwargs.get('pk')
        username_pk_object = CustomUser.objects.get(pk=username_pk)
        friend_pk_object = CustomUser.objects.get(pk=friend_pk)
        friend_pk_object.friendship_offers.remove(username_pk_object)
        return Response(status=204)


class FriendshipConfirmationView(APIView):
    """
    Friendship offers confirmation / Decline
    """
    queryset = CustomUser.objects.all()
    serializer_class = FriendshipSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def patch(self, request, *args, **kwargs):
        """
        Confirmation Friendship Request
        """
        username_pk = self.request.user.pk
        friend_pk = kwargs.get('pk')
        username_pk_object = CustomUser.objects.get(pk=username_pk)
        friend_pk_object = CustomUser.objects.get(pk=friend_pk)

        if friend_pk_object in username_pk_object.friendship_offers.all():
            friend_pk_object.friends.add(username_pk_object)
            username_pk_object.friends.add(friend_pk_object)
            username_pk_object.friendship_offers.remove(friend_pk_object)
            serializer = self.serializer_class(username_pk_object)
            return Response(serializer.data, status=200)
        return Response('Missing application from your new friend', status=400)

    def delete(self, request, *args, **kwargs):
        """
        Decline Friendship Request
        """
        username_pk = self.request.user.pk
        friend_pk = kwargs.get('pk')
        username_pk_object = CustomUser.objects.get(pk=username_pk)
        friend_pk_object = CustomUser.objects.get(pk=friend_pk)
        username_pk_object.friendship_offers.remove(friend_pk_object)
        return Response(status=204)


class FriendshipBreakView(APIView):
    """
    Friendship Break
    """
    queryset = CustomUser.objects.all()
    serializer_class = FriendshipSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def delete(self, request, *args, **kwargs):
        username_pk = self.request.user.pk
        friend_pk = kwargs.get('pk')
        username_pk_object = CustomUser.objects.get(pk=username_pk)
        friend_pk_object = CustomUser.objects.get(pk=friend_pk)
        username_pk_object.friends.remove(friend_pk_object)
        friend_pk_object.friends.remove(username_pk_object)
        return Response(status=204)


class UserInterestUpdateView(RetrieveUpdateDestroyAPIView):
    """
    User's Interest Add, Delete, Get List
    """
    queryset = CustomUser.objects.all()
    serializer_class = UserInterestsSerializer
    permission_classes = (custom_permissions.IsOwnerProfileOrReadOnly,)

    def delete(self, request, *args, **kwargs):
        """
        Delete User Interests

        The method allows to delete objects of interest by the pk list.
        """
        interest_list = self.request.data['interests']
        user_object = self.get_object()
        user_object.user_interests.remove(*interest_list)
        return Response(status=204)

    def update(self, request, *args, **kwargs):
        """
        Update User Interests

        The method allows to Update objects of interest by the pk list.
        """
        interest_list = self.request.data['interests']
        user_object = self.get_object()
        user_object.user_interests.add(*interest_list)
        serializer = self.serializer_class(user_object)
        return Response(serializer.data, status=200)


class UsersSearchListView(ListAPIView):
    """
    Searching Users List
    """
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializerPublic
    # serializer_class = UserSerializerPublic
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (filters.SearchFilter,)
    search_fields = [
        'username',
        'last_name',
        'status_user',
        'about_user',
        'user_interests__title'
    ]
