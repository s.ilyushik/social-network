from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = [
        'email',
        'username',
        'last_name',
        'status_user',
        'about_user',
        'date_of_birth',
        'avatar',
        'profile_bg_image'
    ]
    list_filter = ['date_of_birth']
    model = CustomUser


admin.site.register(CustomUser)
