from django.contrib.auth.models import AbstractUser
from django.db import models
from django.shortcuts import reverse


class CustomUser(AbstractUser):
    """
    Additional fields for the standard Django model.
    """
    avatar = models.ImageField(upload_to='users_uploads/users_avatars', blank=True, verbose_name="User avatar")
    date_of_birth = models.DateField(null=True, verbose_name="User’s birthday")
    about_user = models.TextField(max_length=500, blank=True, verbose_name="About user")
    status_user = models.CharField(max_length=100, blank=True, verbose_name="User status")
    profile_bg_image = models.ImageField(
        upload_to='users_uploads/users_bg_images',
        blank=True,
        verbose_name="Profile bg image"
    )
    user_interests = models.ManyToManyField(
        'common.Interest',
        blank=True,
        related_name='users',
        verbose_name="User interests"
    )
    friendship_offers = models.ManyToManyField(
        'CustomUser', blank=True, related_name='request_for_friendship', verbose_name="User friendship offers"
    )
    friends = models.ManyToManyField(
        'CustomUser',
        blank=True,
        related_name='accepted_friends',
        verbose_name="User friends"
    )

    def get_absolute_url(self):
        return reverse('users_profile_url', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('user_profile_update_url', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}'.format(self.username)
