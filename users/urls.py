from django.urls import path
from .views import (
    SignUp,
    ProfileDetail,
    FriendshipManagement,
    OwnProfileUpdate,
    AddOrRemoveInterestsInProfile,
)


urlpatterns = [
    path('profile/<int:pk>/', ProfileDetail.as_view(), name="users_profile_url"),
    path('profile/<int:pk>/friendship/', FriendshipManagement.as_view(), name="friendship_management_url"),
    path('profile/<int:pk>/update/', OwnProfileUpdate.as_view(), name='user_profile_update_url'),
    path('signup/', SignUp.as_view(), name='signup'),
    path('interestsupd/<int:pk>/', AddOrRemoveInterestsInProfile.as_view(), name="interests_add_or_remove_url")
]
