from django.test import TestCase
from django.urls import reverse

from common.models import Post, Interest
from users.models import CustomUser


class TestProfileDetail(TestCase):
    """Test ProfileDetail View Methods"""
    @classmethod
    def setUpTestData(cls):
        # Create 2 profiles
        number_of_profiles = 2
        for profile_num in range(number_of_profiles):
            CustomUser.objects.create_user(
                username='User_1%s' % profile_num,
                email='%s@1.com' % profile_num,
                password='1234567'
            )
        post = Post.objects.create(body='Body test', username=CustomUser.objects.get(pk=1))
        post.save()

    def test_view_existence_of_form_and_posts(self):
        profile = CustomUser.objects.get(pk=1)
        resp = self.client.get(reverse('users_profile_url', args=[profile.pk]))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp.context['form_of_comment'], 'Response has no form of comment creating!')
        self.assertTrue(resp.context['posts'], "Response has no user's posts")

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='User_10', password='1234567')
        profile = CustomUser.objects.get(pk=1)
        resp = self.client.get(reverse('users_profile_url', args=[profile.pk]))
        self.assertEqual(str(resp.context['user']), 'User_10')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'users_profile.html')


class TestOwnProfileUpdate(TestCase):
    """Test OwnProfileUpdate View Methods"""
    def setUp(self):
        profile1 = CustomUser.objects.create_user(username='Testuser1', email='1@1.com', password='1234567')
        profile1.save()
        profile2 = CustomUser.objects.create_user(username='Testuser2', email='2@1.com', password='1234567')
        profile2.save()
        self.client.logout()

    def test_logged_in_successful_editing(self):
        profile = CustomUser.objects.get(pk=1)
        self.client.login(username='Testuser1', password='1234567')
        resp = self.client.post(
            '/users/profile/' + str(profile.pk) + '/update/',
            {'email': '111@1.com', 'username': 'Testuser1', 'date_of_birth': '2020-03-02'})
        self.assertRedirects(resp, '/users/profile/%s/' % str(profile.pk))
        self.assertEqual(resp.status_code, 302)
        self.assertEqual('111@1.com', CustomUser.objects.get(pk=1).email, "Failed to edit 'email' field!")

    def test_denying_editing_not_own_profile(self):
        # This is someone else's profile for an authorized user
        profile = CustomUser.objects.get(pk=2)
        self.client.login(username='Testuser1', password='1234567')
        resp = self.client.post(
            '/users/profile/' + str(profile.pk) + '/update/',
            {'email': '111@1.com', 'username': 'Testuser1', 'date_of_birth': '2020-03-02'})
        self.assertEqual(
            resp.status_code,
            403,
            "This action should call a 403 code (the user does not have the right to edit someone else's profile)"
        )


class TestFriendshipManagement(TestCase):
    """Test FriendshipManagement View Methods"""
    def setUp(self):
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        test_user2 = CustomUser.objects.create_user(username='testuser2', email='222@22.com', password='1234567')
        test_user2.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.post(reverse('friendship_management_url', args=[1]))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/users/login/?next=/users/profile/1/friendship/')

    def test_friendship_request_incorrect_id(self):
        self.client.login(username='testuser1', password='1234567')
        future_friend_profile = CustomUser.objects.get(pk=2)
        with self.assertRaises(CustomUser.DoesNotExist):
            wrong_id = 10
            self.client.post(
                '/users/profile/%s/friendship/' % future_friend_profile.pk,
                data={'user_action': 'friendship_request', 'friendship_request_recipient_id': wrong_id}
            )

    def test_friendship_request_success(self):
        self.client.login(username='testuser1', password='1234567')
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'friendship_request', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(future_friend_profile.friendship_offers.get(username='testuser1').username, 'testuser1')

    def test_friendship_request_content_data(self):
        self.client.login(username='testuser1', password='1234567')
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'friendship_request', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(
            resp.content.decode(),
            '{"status": "ok", "data": "friendship_request_done"}'
            , 'Wrong status or data!'
        )

    def test_friendship_request_cancel_success_content_data__success(self):
        self.client.login(username='testuser1', password='1234567')
        # Step1: authorized testuser1 sends a friendship request to testuser2
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'friendship_request', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        # Step2: authorized testuser1 sends a friendship request cancel to testuser2.
        # Previously submitted request will be canceled.
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={
                'user_action': 'friendship_request_cancel',
                'friendship_request_recipient_id': future_friend_profile.pk
            }
        )
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(
            future_friend_profile.friendship_offers.exists(),
            f"{CustomUser.objects.get(pk=1)}'s friendship request has not been deleted!"
        )
        self.assertEqual(
            resp.content.decode(),
            '{"status": "ok", "data": "friendship_request_cancel_done"}'
            , 'Wrong status or data!'
        )

    def test_friendship_confirmation_success_content_data__success(self):
        self.client.login(username='testuser1', password='1234567')
        # Step1: authorized testuser1 sends a friendship request to testuser2
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'friendship_request', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.client.logout()
        self.client.login(username='testuser2', password='1234567')
        # Step2: authorized testuser2 confirms friendship request
        future_friend_profile = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'friendship_confirmation', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.assertEqual(resp.status_code, 200)
        # Users will be entered mutually in the friend fields.
        # The request from the friendship request field will be removed.
        self.assertEqual(CustomUser.objects.get(pk=1).friends.get(username='testuser2').username, 'testuser2')
        self.assertEqual(CustomUser.objects.get(pk=2).friends.get(username='testuser1').username, 'testuser1')
        self.assertFalse(
            future_friend_profile.friendship_offers.exists(),
            f"{CustomUser.objects.get(pk=2)}'s friendship request has not been deleted!"
        )
        self.assertEqual(
            resp.content.decode(),
            '{"status": "ok", "data": "friendship_confirmation_done"}'
            , 'Wrong status or data!'
        )

    def test_break_friendship_success_content_data__success(self):
        # Create a friendship between two users
        CustomUser.objects.get(pk=1).friends.add(CustomUser.objects.get(pk=2))
        CustomUser.objects.get(pk=2).friends.add(CustomUser.objects.get(pk=1))
        self.client.login(username='testuser1', password='1234567')
        # Authorized testuser1 sends a break friendship request to testuser2
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'break_friendship', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(
            CustomUser.objects.get(pk=1).friends.exists(),
            f"{CustomUser.objects.get(pk=2)}'s friend has not been deleted!"
        )
        self.assertFalse(
            CustomUser.objects.get(pk=2).friends.exists(),
            f"{CustomUser.objects.get(pk=1)}'s friend has not been deleted!"
        )
        self.assertEqual(
            resp.content.decode(),
            '{"status": "ok", "data": "break_friendship_done"}'
            , 'Wrong status or data!'
        )

    def test_reject_friendship_offer_success_content_data__success(self):
        # Create a friendship offer from testuser2 to testuser1
        CustomUser.objects.get(pk=1).friendship_offers.add(CustomUser.objects.get(pk=2))
        self.client.login(username='testuser1', password='1234567')
        # Authorized testuser1 sends a reject friendship offer request to testuser2
        future_friend_profile = CustomUser.objects.get(pk=2)
        resp = self.client.post(
            '/users/profile/%s/friendship/' % future_friend_profile.pk,
            data={'user_action': 'reject_friendship_offer', 'friendship_request_recipient_id': future_friend_profile.pk}
        )
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(
            CustomUser.objects.get(pk=1).friendship_offers.exists(),
            f"{CustomUser.objects.get(pk=2)}'s friendship offer has not been deleted!"
        )
        self.assertEqual(
            resp.content.decode(),
            '{"status": "ok", "data": "reject_friendship_offer_done"}'
            , 'Wrong status or data!'
        )


class TestAddOrRemoveInterestsInProfile(TestCase):
    """Test AddOrRemoveInterestsInProfile View Methods"""
    def setUp(self):
        # Create 5 interests
        number_of_interests = 5
        for interest_num in range(number_of_interests):
            Interest.objects.create(title='test interest%s' % interest_num)
        test_user1 = CustomUser.objects.create_user(username='testuser1', email='111@11.com', password='1234567')
        test_user1.save()
        self.client.logout()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('interests_add_or_remove_url', args=[1]))
        self.assertRedirects(resp, '/users/login/?next=/users/interestsupd/1/')

    def test_view_add_new_interests_correct_redirect(self):
        self.client.login(username='testuser1', password='1234567')
        user_profile = CustomUser.objects.get(pk=1)
        resp = self.client.post(
            '/users/interestsupd/%s/' % user_profile.pk,
            {'user_interests': ['1'], 'add_interest': ['1']}
        )
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(CustomUser.objects.get(pk=1).user_interests.get(pk=1), Interest.objects.get(pk=1))
        self.assertRedirects(resp, '/')

    def test_view_delete_interests_correct_redirect(self):
        self.client.login(username='testuser1', password='1234567')
        user_profile = CustomUser.objects.get(pk=1)
        # Step 1: add 2 interests
        self.client.post(
            '/users/interestsupd/%s/' % user_profile.pk,
            {'user_interests': ['1', '2'], 'add_interest': ['1', '2']}
        )
        # Step 2: 1 interest has been removed
        resp = self.client.post(
            '/users/interestsupd/%s/' % user_profile.pk,
            {'user_interests': ['1'], 'delete_interest': ['1']})
        self.assertEqual(resp.status_code, 302)
        with self.assertRaises(Interest.DoesNotExist):
            CustomUser.objects.get(pk=1).user_interests.get(pk=1)
        self.assertRedirects(resp, '/')
