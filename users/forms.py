from django import forms
from django.contrib.auth import forms as auth_forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils import timezone
from django.core.exceptions import ValidationError

from .models import CustomUser


# Defining a date range for a year selection field
BIRTH_YEAR_CHOICES = range(timezone.now().year - 110, timezone.now().year + 1)


class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(
        required=True,
        widget=forms.EmailInput(attrs={'class': 'form-control validate', 'placeholder': 'Email *'})
    )
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Create your Password *'})
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Repeat your Password *'})
    )
    terms_agree = forms.CharField(widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('username', 'last_name', 'email', 'avatar', 'date_of_birth')

        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First name *'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last name'}),
            'avatar': forms.ClearableFileInput(attrs={'class': 'form-control-file'}),
            'date_of_birth': forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES, attrs={'class': 'form-control-sm col-4'})
        }

    def clean_terms_agree(self):
        terms_agree = self.cleaned_data['terms_agree']

        if terms_agree != 'True':
            raise ValidationError('To register, you must check the box and thereby accept the terms and conditions!')
        return terms_agree


class CustomUserChangeForm(UserChangeForm):
    exclude = ['password']
    password = auth_forms.ReadOnlyPasswordHashField(
        label="Password",
        help_text="Raw passwords are not stored, so there is no way to see this user's password, "
                  "but you can change the password ' \'using <a href=\"password/\">this form</a>."
    )

    class Meta(UserChangeForm.Meta):
        model = CustomUser
        fields = ('username', 'email', 'avatar', 'profile_bg_image', 'status_user', 'about_user', 'date_of_birth')

        widgets = {
            'avatar': forms.ClearableFileInput(attrs={'class': 'form-control-file'}),
            'profile_bg_image': forms.ClearableFileInput(attrs={'class': 'form-control-file'}),
            'status_user': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your Status'}),
            'about_user': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'About You', 'rows': '5'}),
            'date_of_birth': forms.SelectDateWidget(years=BIRTH_YEAR_CHOICES)
        }


class InterestForm(forms.ModelForm):
    """
    Form to add or remove interests in a user profile
    """
    class Meta:
        model = CustomUser
        fields = ['user_interests']

        widgets = {
            'user_interests': forms.CheckboxSelectMultiple()
        }

    def clean_user_interests(self):
        user_interests = self.cleaned_data['user_interests']

        if not user_interests:
            raise ValidationError('You must select something and click add or delete it.')
        return user_interests
